# 2018-2019 | A la bonne ferme x Latitudes

_

## Contexte
* A la bonne ferme est une petite ferme de Picardie dont L'actiovité s'axe autour de trois objectifs: Insertion, Permaculture et Techs
* A la bonne ferme s'engage à favoriser la réinsertion de personnes en difficulté.
* https://www.alabonneferme.fr

## À propos du projet
* Améliorer la productivité de la ferme et libérer du temps au chef de culture
* Mettre en place un système automatique d'ouverture de serre afin de réguler la température à l'intérieur de la serre à distance.
* Le cadre dans lequel cela s'inscrit : Tech for Good Explorers (description à copier-coller directement).

## Perspectives d'évolution
* Conception d'un algorithme complet de gestion de l'ouverture de la serre.
* Travail sur les données récupérées par les capteurs pour optimiser l'ouverture et donner des conseils au chef de culture.

## Sommaire
* Ce repo est composé d'une partie code dans laquelle on trouve tout le code  du projet et d'une partie documentation qui représente toute la biblio que l'on a utilisé pour le projet

## Contact
* Pierre Wallaert: 06.95.62.26.62
