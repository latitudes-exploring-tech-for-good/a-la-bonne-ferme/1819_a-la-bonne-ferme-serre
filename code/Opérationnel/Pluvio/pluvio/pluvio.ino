unsigned long timeRun;
unsigned long timeLast;
float periode;

/***********Variables pour la quantité dde pluie*****************************/
int pluvio = 3;                           //declare un var pluvio qui = 3
float q_pluie = 0.00;                     //var qui contiendra la quantité de pluie
float auget = 0.0035;                     //quantité que un auget peut contenir 3.5 ml / 0.0035 l
float surfacePluvio = 0.0055;             //surface de réception du capteur de pluie 55 cm² / 0.0055 m²
int pluvioTickCompteur = 0;               //variable pour le comptage du nombre de "TICKS" du capteur ILS du pluvio
 

void PluvioTicks();                       //Déclare le SP PluvioTicks

void setup(){
  Serial.begin(9600);
  Serial.print("go");
  pinMode(pluvio, INPUT);             //definit la pin pluvio(3) comme une entrée
 
 attachInterrupt(digitalPinToInterrupt(pluvio), PluvioTicks, RISING);     //dès que la broche pluvio(3) changera d'etat (RISING = état BAS vers HAUT (front montant)) on appelle le SP PluvioTicks
 
}
void loop()
{
 
  timeRun = millis();                                 //affecte le temps depuis lequel le programme tourne à la var timeRun
}

void PluvioTicks ()
{
  pluvioTickCompteur ++ ;                                //ajoute 1 au compteur de TICK du pluvio
  q_pluie = (pluvioTickCompteur * auget)/surfacePluvio;  //application de la formule de la quantité de pluie
  Afficher();                                            //appelle le SP Afficher
}

void Afficher()
{
  Serial.println(q_pluie);              //affiche la valeur de la quantité de pluie
}
