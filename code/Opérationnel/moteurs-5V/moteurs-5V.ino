
/* ouverture/fermeture pour les 2 moteurs 5V, selon température ou en mode manuel avec 2 boutons pour chaque moteur ; 
système d'alarme (lumineux et sonore) ; bouton de changement de mode */


// bibliothèques pour le capteur

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme; // I2C

//bibliotheque communication
#include "JsonAndGSM.h"


unsigned long delayTime;


// définition des pins servant au contrôle des moteurs dans une matrice, de 3 colonnes et d'autant de lignes que de moteurs à contrôler :
// colonnes : pinPWM puis les 2 pins de contrôle (du sens) du moteur

int pinMoteurs[][3]={{44,42,43},
                    {46,48,49}};

int nbremoteur=2; //à modifier selon le nombre de moteurs à contrôler


// défintion des pins de conrôle du signal lumineux pour chaque moteur

int pinAlarm[]={45,47};



//définition du pin lié au bouton de changement de mode surplace

const int pinManual=40; //passage en mode manuel


//définition du pin de contrôle de la LED allumée lorsqu'on est en mode surplace

const int pinLedManual=41;


//définition des pins liés aux boutons d'ouverture et de fermeture de chaque moteur en mode manuel par une matrice
//colonne 1 : pin des boutons demandant ouverture | colonne 2 : pin des boutons demandant fermeture
//nbre de lignes = nbre de moteurs à contrôler

int pinBoutonManual[][2]={{22,23},
                          {24,25}};


//tableau contenant l'état d'ouverture de chacun des moteurs (pourcentage, entre 0 et 100, par paliers de 25%)

int tauxOuverture[]={0,0};




void setup() {

  // définition des modes des différents pins utilisés


  for (int i=0;i<nbremoteur;i++) {
    for (int j=0;j<=3;j++){
      pinMode(pinMoteurs[i][j],OUTPUT); //contrôle des moteurs
    }
    pinMode(pinAlarm[i],OUTPUT); //contrôle des systèmes d'alarme
    for (int k=0;k<=1;k++){
      pinMode(pinBoutonManual[i][k],INPUT_PULLUP); //ouverture/fermeture en mode manuel
    }
  }
  pinMode(pinLedManual,OUTPUT); //LED mode manuel
  pinMode(pinManual,INPUT_PULLUP); //changement de mode

  Serial.begin(9600);
    Serial.println(F("BME280 test"));

    bool status;
    
    // paramétrage du capteur de température et humidité (default settings)
    status = bme.begin();  
    if (!status) {
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
        //while (1);
    }
    
    Serial.println("-- Default Test --");
    delayTime = 1000;

    Serial.println();
}

//mode manuel sur place
int surplace=0;


<<<<<<< HEAD
//données à obtenir à récupérer du json, fixées pour l'instant arbitrairement
  int manual= 1;      //=1 si c'est en mode manuel à distance
  int min_temp=25;    // temperature en dessous de laquelle la serre se ferme en mode automatique
  int max_temp=22;    //température au dessus de laquelle la serre s'ouvre en mode automatique
  int consigne1=25;   // consigne d'ouverture pour le moteur 1 en mode manuel a disatnce
  int consigne2=50;   // consigne d'ouverture pour le moteur 2 en mode manuel a disatnce
=======
//initialisation des données à obtenir à récupérer du json, en cas de problème de connexion
  int manual= 1;
  int refresh=60;
  int min_temp=25;
  int max_temp=22;
  int consigne1=25;
  int consigne2=50;
>>>>>>> f2677f462feb1d855a0a45adf9c1c64f5a9f68ae

int consigne[]={consigne1,consigne2};


void loop() {




Serial.println("mode actuel :");//Affichage du mode actuel
Serial.print("Surplace:");
Serial.println(surplace);    
Serial.print("Manual:");
Serial.println(manual);
Serial.print("Ouverture côté 1:");  
Serial.println(tauxOuverture[0]);
Serial.print("Ouverture côté 2:"); 
Serial.println(tauxOuverture[1]);
  
      bool modifmode=digitalRead(pinManual); //modifmode vaut 0 si on a appuyé sur le bouton pour changer de mode
      
      if (!modifmode) { //si le bouton de changement de mode est appuyé
        surplace=!surplace; //inversion de la valeur de manual
        if (surplace){
           digitalWrite(pinLedManual,HIGH); //allumage de la LED de mode surplace
           delay(3000);
        }
      }

   Serial.println(modifmode);
   
     if (surplace){ //si  on est en mode surplace
    
        digitalWrite(pinLedManual,HIGH); //allumage de la LED de mode surplace

        bool consigneBoutonManual[nbremoteur][2];
        for (int i = 0; i<nbremoteur ;i++){
            consigneBoutonManual[i][0]=digitalRead(pinBoutonManual[i][0]);  //vaudra 0 si le bouton d'ouverture du moteur i est appuyé, auquel cas il faudra ouvrir
            consigneBoutonManual[i][1]=digitalRead(pinBoutonManual[i][1]); //vaudra 0 si le bouton de fermeture du moteur i est appuyé, auquel cas il faudra fermer
        }

//        Serial.print(consigneBoutonManual[0][0]); //Utilisé pour connaitre les botons sur lesquels on a appuyé
//        Serial.println(consigneBoutonManual[0][1]);
//        Serial.print(consigneBoutonManual[1][0]);
//        Serial.println(consigneBoutonManual[1][1]);
        
        for (int i=0; i<nbremoteur ;i++){

            if (!consigneBoutonManual[i][0]){
              
              actionMoteur(i,tauxOuverture[i]+20); //Si le bouton ouverture est appuyé, on ouvre le moteur de 20% supplémentaires
              tauxOuverture[i]=tauxOuverture[i]+20;
            }

            else if (!consigneBoutonManual[i][1]){
              actionMoteur(i,tauxOuverture[i]-20); //Si le bouton ouverture est appuyé, on ouvre le moteur de 20% en moins
              tauxOuverture[i]=tauxOuverture[i]-20;
            }
        }
     }
   
    
    else { //mode contrôle à distance
digitalWrite(pinLedManual,LOW); //extinction de la LED pour les modes à distance

<<<<<<< HEAD
startArduinoClient();   //On lance le processus GSm
String json=downloadJson();   //On télécharge le json sous forme d'une chaine de caractères
//Serial.println(json);
JsonObject& result=convert(json); //On le convertit en JsonObject&
//Serial.println("Je suis ici");
int manual=result["manual_mode"];   //On récupère les différents paramètres dans les différentes variables
int min_temp=result["tempmin"];
=======
// récupération des données depuis le json en ligne

      startArduinoClient();
updatingData(5,5);
startArduinoClient();
String json=downloadJson();
Serial.println(json);
JsonObject& result=convert(json);
Serial.println("Je suis ici");
int manual=result["manual_mode"];
int tempmin=result["tempmin"];
>>>>>>> f2677f462feb1d855a0a45adf9c1c64f5a9f68ae
int max_temp=result["tempmax"];
int consigne1=result["nord"];
int consigne2=result["sud"];
int consigne[]={consigne1,consigne2};
int est=result["est"];
int ouest=result["ouest"];
//Serial.println(manual);   //Bloc qui était utilisé pour le contrôle des données récupérées
//Serial.println(min_temp);
//Serial.println(max_temp);
//Serial.println(consigne[0]);
//Serial.println(consigne[1]);
//Serial.println(est);
//Serial.println(ouest);

      if (!manual){ //mode totalement automatique, en fonction de la température seulement

    float temp=bme.readTemperature();   //On récupère la température actuelle
    
    if (temp>max_temp) {    //Si elle est supérieur à max_temp, on ouvre plus la serre
      actionMoteur(0,tauxOuverture[0]+20);
      tauxOuverture[0]=tauxOuverture[0]+20;
      actionMoteur(1,tauxOuverture[1]+20);
      tauxOuverture[1]=tauxOuverture[1]+20;
  }

  else if (temp<min_temp) {     //Si elle est supérieur à max_temp, on ouvre plus la serre
      actionMoteur(0,tauxOuverture[0]-20);
      tauxOuverture[0]=tauxOuverture[0]-20;
      actionMoteur(1,tauxOuverture[1]-20);
      tauxOuverture[1]=tauxOuverture[1]-20;
  }
//  printValues(); //affichage des conditions de température et d'humidité
      }

    else{ //s'adapte aux consignes données
      for (int i=0;i<nbremoteur;i++) {
        actionMoteur(i,consigne[i]); 
        
        if (tauxOuverture[i]<consigne[i]){
          tauxOuverture[i]=tauxOuverture[i]+20;
        }
        else if (tauxOuverture[i]>consigne[i]){
          tauxOuverture[i]=tauxOuverture[i]-20;
        }
      }
    }
    float temp=bme.readTemperature();
    startArduinoClient();
    updatingData(temp,manual);    //On envoie sur Thinkspeak les valeurs de tempéreture et de manual
    }
    
    

  printValues(); //Affiche les valeurs récupérées par bme280
}




// fonction de contrôle d'un moteur

//@to do : intégrer la mise à jour du taux d'ouverture à la fonction action Moteur plutot que de la laisser à chaque fois avec l'appel à actionMoteur

//@to do : gérer différentes ouvertures d'un coup (ie pouvoir ouvrir d'un coup en entier un mur, plutot que palier par palier, ce qui allume/éteint l'alarme 5 fois de suite 

void actionMoteur(int moteur, int tauxcible) { //moteur: numéro du moteur à actionner ; taux cible : ouverture voulue du pan  
  int pinP=pinMoteurs[moteur][0];
  int pin1=pinMoteurs[moteur][1];
  int pin2=pinMoteurs[moteur][2];
  analogWrite(pinP,100);

  
  if (tauxOuverture[moteur]<tauxcible) {
    digitalWrite(pinAlarm[moteur],1);
    digitalWrite(pin1,0);
    digitalWrite(pin2,1);
    delay(5000);
  }
  
  else if (tauxOuverture[moteur]>tauxcible) {
    digitalWrite(pinAlarm[moteur],1);
    digitalWrite(pin2,0);
    digitalWrite(pin1,1);
    delay(5000);
  }
  
  digitalWrite(pinAlarm[moteur],0); 
  digitalWrite(pin1,0);
  digitalWrite(pin2,0);   
  
}

  
void printValues() { //affichage des valeurs de température et pression
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();
}
