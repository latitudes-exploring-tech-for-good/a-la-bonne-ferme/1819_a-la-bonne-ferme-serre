#include "Arduino.h"
#include <ArduinoJson.h>
#include <GSM.h>

char server[] = "alabonneferme2.emerya.info";
char path[] = "/test.json";
int port = 80; // port 80 is the default for HTTP,443 for https



void startArduinoClient(){
  // APN data
#define GPRS_APN       "mms.bouygtel.com" // replace your GPRS APN
#define GPRS_LOGIN     ""    // replace with your GPRS login
#define GPRS_PASSWORD  "" // replace with your GPRS password

// initialize the library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess;
  //Start the connection to GPRS thanks to GSM Module
    Serial.println("Starting Arduino web client.");
  // connection state
  boolean notConnected = true;

  // After starting the modem with GSM.begin()
  // attach the shield to the GPRS network with the APN, login and password
  while (notConnected) {
    if ((gsmAccess.begin("0000") == GSM_READY) &
        (gprs.attachGPRS("mms.bouygtel.com", "", "") == GPRS_READY)) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

  Serial.println("connecting...");
}

void updatingData(int temp, int wind){
  // APN data
#define GPRS_APN       "mms.bouygtel.com" // replace your GPRS APN
#define GPRS_LOGIN     ""    // replace with your GPRS login
#define GPRS_PASSWORD  "" // replace with your GPRS password

// initialize the library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess;
  //Update temp and wind on thinkspeak
   char server[] = "api.thingspeak.com";
    // Construct Thingspeak URL.
    String path = "/update?api_key=XCBTBCHC1HGGHZ4F&field1=";
    path.concat(String(temp));
    path.concat("&field2=");
    path.concat(String(wind));

      
    // Send data to thingspeak
    if (client.connect(server, port)) {
      Serial.println("Connected");
      Serial.print("Calling : ");
      Serial.print(server);
      Serial.println(path);      
      // Make a HTTP request:
      client.print("GET ");
      client.print(path);
      client.println(" HTTP/1.1");
      client.print("Host: ");
      client.println(server);
      client.println("Connection: close");
      client.println();
      Serial.println("Sent");
      Serial.println("Deconnecting.");
      delay(1000);
      
      Serial.println("Deconnected.");
    } else {
      // Didn't get a connection to the server:
      Serial.println("connection failed for sending temperature");
      // Restart GSM Network in case we lost connection.
      client.stop();
    }      
  	
  
}

String downloadJson(){
  // APN data
#define GPRS_APN       "mms.bouygtel.com" // replace your GPRS APN
#define GPRS_LOGIN     ""    // replace with your GPRS login
#define GPRS_PASSWORD  "" // replace with your GPRS password

// initialize the library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess;
   //Go on the website to read the data of a Json

  // if you get a connection, report back via serial:
  if (client.connect("alabonneferme2.emerya.info", 80)) {
    Serial.println("connected");

    // Make a HTTP request:
    client.print("GET ");
    client.print("/test.json");
    client.println(" HTTP/1.1");
    client.print("Host: ");
    client.println("alabonneferme2.emerya.info");
    client.println("Connection: close");
    client.println();
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
 String json="";
  
  while(!client.available()){
    delay(10);
  }
  int start =0;
  while(client.available()){
    //Serial.print("passage dans la boucle");
    char next= client.read();
    //Serial.print(next);
    //Serial.print(start);
    if(String(next) ==  "{"){
      start=1;
    }
    String next_2= String(next);
    if(start==1){
      json=json+next_2;
    }
    
    //Serial.print(json);
    
    }

 client.stop();
 //Serial.print(json);
 return(json);
}

JsonObject& convert(String json){
 

  //Convert a string into a Buffer readable 
  StaticJsonBuffer<200> jsonBuffer;
  
  
  
  JsonObject& root = jsonBuffer.parseObject(json);

  if(!root.success()) {
    Serial.println("parseObject() failed");
    //return false;
  }
  return(root);
}
