<?php
/** 
 * @file
 * Contains \Drupal\Temperature\Form\TemperatureForm_Manu
 */
namespace Drupal\Temperature\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\core\modules\file\FileInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TemperatureForm_Manu extends FormBase{
	/**
	* {@inheritdoc}
	*/
	public function getFormId(){
		return 'Temperature_form_Manu';
	}
	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state) {
		
		$form['sud']= array(
		  '#type' => 'textfield',
		  '#title' => t('Niveau ouverture sud: '),
		);
		$form['ouest']= array(
		  '#type' => 'textfield',
		  '#title' => t('Niveau ouverture ouest: '),
		);
		$form['nord']= array(
		  '#type' => 'textfield',
		  '#title' => t('Niveau ouverture nord: '),
		);
		$form['est']= array(
		  '#type' => 'textfield',
		  '#title' => t('Niveau ouverture est: '),
		);
		$form['submitmanu'] = array(
		  '#type' => 'submit',
		  '#value' => t('Valider'),
		  '#button_type' => 'success',
		  //'#attributes' => array('onclick' => 'this.form.target="_blank";return true;'),
		);
		return $form;
	}
	/**
		* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state){
        $data = file_get_contents('public://example.json');
        $data=$data . ',\"sud\":' . $form_state->getValue('sud') . ',\"ouest\":' . $form_state->getValue('ouest') . ',\"nord\":' . $form_state->getValue('nord') . ',\"est\":' . $form_state->getValue('est') . ',"min_temp":20,"max_temp":20}';	
		file_save_data($data,$dest = 'public://example.json',$replace = FILE_EXISTS_REPLACE);
		drupal_set_message('Les nouveaux niveaux d\'ouverture sont prises en compte'); 
		$response = new RedirectResponse('/Alabonneferme/table_controle');
		$response->send();
	}
}
 