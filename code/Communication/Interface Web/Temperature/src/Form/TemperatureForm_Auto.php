<?php
/** 
 * @file
 * Contains \Drupal\Temperature\Form\TemperatureForm_Auto
 */
namespace Drupal\Temperature\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\core\modules\file\FileInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TemperatureForm_Auto extends FormBase{
	/**
	* {@inheritdoc}
	*/
	public function getFormId(){
		return 'Temperature_form_Auto';
	}
	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state) {
		
		$form['tempmin']= array(
		  '#type' => 'textfield',
		  '#title' => t('Temperature Min: '),
		);
		$form['tempmax']= array(
		  '#type' => 'textfield',
		  '#title' => t('Temperature Max: '),
		);
		$form['submit'] = array(
		  '#type' => 'submit',
		  '#value' => t('Valider'),
		  '#button_type' => 'success',
		);
		return $form;
	}
	/**
		* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state){	
		$data = file_get_contents('public://example.json');
		$temperaturemin = $form_state->getValue('tempmin');
		$temperaturemax = $form_state->getValue('tempmax');
		$data = $data . '\"min_temp\": ' . $temperaturemin . ',\"max_temp\": ' . $temperaturemax . '}';
		file_save_data($data,$dest = 'public://example.json',$replace = FILE_EXISTS_REPLACE);
		drupal_set_message('La temperature d\'ouverture automatique est mise à : Min= ' . $temperaturemin . 'Max= ' . $temperaturemax); 
		$response = new RedirectResponse('/Alabonneferme/table_controle');
		$response->send();
	}
}
 