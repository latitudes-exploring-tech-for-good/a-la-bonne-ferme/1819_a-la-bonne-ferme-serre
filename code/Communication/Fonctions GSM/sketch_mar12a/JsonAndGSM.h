/*
  JsonAndGSM.h - Library for converting an online Json into a buffer
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/

#include "Arduino.h"
#include <GSM.h>
#include <ArduinoJson.h>



void startArduinoClient();
void updatingData(int temp , int wind);
String downloadJson();
JsonObject& convert(String json);
    
  



