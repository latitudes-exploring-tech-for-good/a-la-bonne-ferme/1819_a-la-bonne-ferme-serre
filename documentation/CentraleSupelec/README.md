# 2018/2109 | A la bonne ferme x Latitudes


Chaque document présent dans ce dossier sera nommé "AAMMJJ - Nom du document" pour faciliter la compréhension.

## Les jalons

* Jalon I: Prise en main du projet, équivalent de la fin de phase découverte chez Latitudes
* Jalon II: Bibliographie sur le sujet
* Jalon III: Livrable intermédiaire/final

## Contexte
* Projet Inno, CentraleSupélec, 2ème année cursus Ingénieur Centralien
* Nous sommes un groupe de 4. Le temps dédié à ce projet est de 1 jour par semaine pendant 1 an.
* Le projet est suivi via des Jalons par l'établissement, 3 jalons par semestres, et une soutenance à la fin de chaque semestre.


Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès au code source se fait via le dossier ["code"](../code/).
